# TrackAndTrustConfig

This is a repository that is used to update the Track and trust app.

The Raspberry nodes will have a cron job that uses the 'ansibe-pull' module and thus update their own configuration based on the Ansible playbook that resides in the repository.